/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;


import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.shape.Polygon;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import saf.components.AppWorkspaceComponent;
import mv.MapViewerApp;
import mv.data.DataManager;

/**
 *
 * @author McKillaGorilla
 * @author Yushan Zhou
 */
public class Workspace extends AppWorkspaceComponent {
    MapViewerApp app;
    
    Group root;
    
    DataManager dataManager;
    
    static double sceneWidth;
    static double sceneHeight;
    
    static double sceneCenterX;
    static double sceneCenterY;
    
    ArrayList<Line> gridline;
    boolean linesOn = false;
            
    private double scaleSize = 1.0;
    
    public Workspace(MapViewerApp initApp) {
        app = initApp;
        
        workspace = new Pane();
        
        dataManager = (DataManager)app.getDataComponent();
        sceneWidth = app.getGUI().getPrimaryScene().getWidth();
        sceneHeight = app.getGUI().getPrimaryScene().getHeight();
        sceneCenterX = sceneWidth/2;
        sceneCenterY = sceneHeight/2;
        gridline = new ArrayList<Line>();
        
        setupHandlers();
       
    }
    
    public static double getSceneWidth(){
        return sceneWidth;
    }
    
    public static double getSceneHeight(){
        return sceneHeight;
    }
    
    public void removeRoot(){
        workspace.getChildren().clear();
    }

    @Override
    public void reloadWorkspace() {
        
        //avoid overflow toolbar;
        Rectangle clip = new Rectangle(app.getGUI().getPrimaryScene().getWidth(), app.getGUI().getPrimaryScene().getHeight());
        clip.setTranslateY(5);
        workspace.setClip(clip);
        
        //draw polygons
        root = new Group();
        
        for(int i = 0; i < dataManager.getPolygonList().size(); i++) {
            Polygon polygon = new Polygon();
            polygon.getPoints().addAll(dataManager.getPolygonList().get(i).toDisplay());
            polygon.setFill(Color.valueOf("#008000"));
            polygon.setStroke(Color.valueOf("#000000"));
            root.getChildren().add(polygon);
        }
        
        //blue background: color of LIGHTBLUE
        workspace.setStyle("-fx-background-color: #ADD8E6");
        
        //grid lines
        Line equator = new Line(0, sceneCenterY, sceneWidth, sceneCenterY);
        gridline.add(equator);
        Line meridian = new Line(sceneCenterX, 0, sceneCenterX, sceneHeight);
        gridline.add(meridian);       
        Line dateline_left = new Line(0, 0, 0, sceneHeight);
        gridline.add(dateline_left);
        Line dateline_right = new Line(sceneWidth, 0, sceneWidth, sceneHeight);
        gridline.add(dateline_right);
        
        for(int i = 0; i < gridline.size(); i++){
            gridline.get(i).setStroke(Color.WHITE);
        }
        
        for(int i = 1; i < 12; i++){
            if(i != 6){
                Line longitude = new Line(sceneWidth/ 12 * i, 0, sceneWidth/ 12 * i, sceneHeight);
                longitude.setStroke(Color.LIGHTGREY);
                longitude.getStrokeDashArray().addAll(25d, 10d);
                gridline.add(longitude);
            }
        }
        
        for(int i = 1; i < 6; i++){
            if(i != 3){
                Line latitude = new Line(0 , sceneHeight/ 6 * i, sceneWidth, sceneHeight/ 6 * i);
                latitude.setStroke(Color.LIGHTGREY);
                latitude.getStrokeDashArray().addAll(25d, 10d);
                gridline.add(latitude);
            }
        }
        
        linesOn = true;
        for(int i = 0; i < gridline.size(); i++){
            root.getChildren().add(gridline.get(i));
        }
        
        workspace.getChildren().add(root);
        
    }
    
    private void setupHandlers() {
        
        workspace.setOnMouseClicked(e -> {
            if(e.getButton() == MouseButton.PRIMARY){
                double cursorX = e.getSceneX();
                double cursorY = e.getSceneY();
                processZoomIn(cursorX, cursorY);
            }
            
            else if(e.getButton() == MouseButton.SECONDARY){
                processZoomOut();
            }
        
        });
        
        app.getGUI().getAppPane().setOnKeyPressed(e -> {
            
            switch (e.getCode().toString()) {
                case "UP":
                    root.setLayoutY(root.getLayoutY() -200);
                    break;
                case "RIGHT": 
                    root.setLayoutX(root.getLayoutX() + 200);
                    break;
                case "DOWN":
                    root.setLayoutY(root.getLayoutY() + 200);
                    break;
                case "LEFT": 
                    root.setLayoutX(root.getLayoutX() - 200);
                    break;
                case "G":
                    showGrid();
                    break;
            }
            
        });
    }
    
    private void showGrid(){
        if(!linesOn){
            for(int i = 0; i <gridline.size(); i++){
                if(i < 4)
                    gridline.get(i).setStroke(Color.WHITE);
                else
                     gridline.get(i).setStroke(Color.LIGHTGREY);
            }
            linesOn = true;
            
        }
        else{
            for(int i = 0; i < gridline.size(); i++){
                 gridline.get(i).setStroke(Color.TRANSPARENT);
            }
            linesOn = false;
        }
        
    }
    
    private void processZoomIn(double cursorX, double cursorY){
        
        removeRoot();
        
        root.setLayoutX(root.getLayoutX() + (sceneWidth/2 - cursorX));
        root.setLayoutY(root.getLayoutY() + (sceneHeight/2 - cursorY));
        
        root.setScaleX(root.getScaleX() * 2);
        root.setScaleY(root.getScaleY() * 2);
        
//        root.setLayoutX(root.getLayoutX() + (sceneWidth/2 - cursorX));
//        root.setLayoutY(root.getLayoutY() + (sceneHeight/2 - cursorY));
        
//        scaleSize *= 2;
//        for(int i = 0; i < dataManager.getPolygonList().size(); i++) {
//            dataManager.getPolygonList().get(i).setScale(scaleSize);
//        }
//        
       workspace.getChildren().add(root);
    }
    
    private void processZoomOut(){
        
        removeRoot();
        
        root.setScaleX(root.getScaleX() / 2);
        root.setScaleY(root.getScaleY() / 2);
        
        workspace.getChildren().add(root);
        
    }
    
    @Override
    public void initStyle() {
        
        //remove two buttons;
        FlowPane toolbar = (FlowPane) app.getGUI().getAppPane().getTop();
        toolbar.getChildren().remove(0);
        toolbar.getChildren().remove(1);
    }
    
}
