package mv.data;

import java.util.ArrayList;
import java.util.List;
import saf.components.AppDataComponent;
import mv.MapViewerApp;
import mv.gui.Workspace;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    List<PolygonData> polygonList;
    
    MapViewerApp app;
    
    public DataManager(MapViewerApp initApp) {
        app = initApp;
        polygonList = new ArrayList<>();
    }
    
    @Override
    public void reset() {
        polygonList.clear();
        ((Workspace)app.getWorkspaceComponent()).removeRoot();
    }
    
    public List<PolygonData> getPolygonList() {
        return polygonList;
    }
}
