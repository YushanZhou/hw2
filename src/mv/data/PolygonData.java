/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.data;

import java.util.ArrayList;
import java.util.List;
import mv.gui.Workspace;
/**
 *
 * @author Yushan Zhou
 */
public class PolygonData {
    private double scale = 1.0;
    
    private List<Double> points;
	       
    private double sceneWidth;
    
    private double sceneHeight;
    
    
    private double sceneCenterX;
    
    private double sceneCenterY;
    
    private List<Double> display;
    
    private double tempX;
    
    private double tempY;
    
    double cursorX;
    
    double cursorY;
    
    boolean zoomed = false;
    
    double diffX;
    
    double diffY;
    
    public PolygonData(){
        points = new ArrayList<>();
        display = new ArrayList<>();
        sceneWidth = Workspace.getSceneWidth();
        sceneHeight = Workspace.getSceneHeight();
        sceneCenterX = sceneWidth/2;
        sceneCenterY = sceneHeight/2;
    }
    
    public void convert( double value){
        display = new ArrayList<>();
        if(zoomed){
//            diffX = sceneCenterX - cursorX;
//            diffY = sceneCenterY - cursorY;
            
            
            for(int i = 0; i < points.size() - 1; i +=2){
                //tempX = sceneWidth / (360 / scale) * (points + 180 );
                display.add(tempX);
                tempY =  90 - points.get(i + 1) + diffY ;
                //tempY = sceneHeight / (180 / scale) * (90 - tempY);
                display.add(tempY);
            }
                
//            display.add(600.0 + diffX);display.add(200.0 + diffY);
//                display.add(600.0 + diffX);display.add(400.0 +diffY);
//                display.add(800.0 + diffX);display.add(200.0 + diffY);
        } else {
                for(int i = 0; i < points.size() - 1; i +=2){
                    //tempX = points.get(i) + 180;
                    tempX = sceneWidth / (360 / scale) * (points.get(i) + 180);
                    display.add(tempX);
                    //tempY = 90 - points.get(i + 1);
                    tempY = sceneHeight / (180 / scale) * ( 90 - points.get(i + 1));
                    display.add(tempY);
                }
//                display.add(600.0);display.add(200.0);
//                display.add(600.0);display.add(400.0);
//                display.add(800.0);display.add(200.0);
//                
        }
    }
    
    //default initial (i.e. 1x scaling) loading
    public List<Double> toDisplay(){
        convert(scale);
        return display;
    }
        
    //original points from JSON
    public List<Double> getPoints() {
        
        return points;
    }
    
    public void setScale(double value) {
        scale = value;
        zoomed = true;
    }
    
    public void setCursorPosition(double x, double y){
        cursorX = x;
        cursorY = y;
    }
}
