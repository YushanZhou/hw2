/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import mv.data.DataManager;
import mv.data.PolygonData;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
	DataManager dataManager = (DataManager)data;
	dataManager.reset();
        
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
        
	// AND NOW LOAD ALL THE ITEMS
	JsonArray jsonRegionArray = json.getJsonArray("SUBREGIONS");
        
	for (int i = 0; i < jsonRegionArray.size(); i++) {
            int numPolygons = jsonRegionArray.getJsonObject(i).getInt("NUMBER_OF_SUBREGION_POLYGONS");
            JsonArray jsonPolygonArray = jsonRegionArray.getJsonObject(i).getJsonArray("SUBREGION_POLYGONS");
            
            for(int j = 0; j < numPolygons; j++) {
                JsonArray jsonPointArray = jsonPolygonArray.getJsonArray(j);

                PolygonData polygon = new PolygonData();
                for(int k = 0; k < jsonPointArray.size(); k++) {
                    JsonObject jsonPoint = jsonPointArray.getJsonObject(k);
                    polygon.getPoints().add(getDataAsDouble(jsonPoint, "X"));
                    polygon.getPoints().add((getDataAsDouble(jsonPoint, "Y")));
                }
                dataManager.getPolygonList().add(polygon);
            }
	}
        
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
